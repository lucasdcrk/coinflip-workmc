@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Coinflip</div>

                <div class="card-body">
                    @if(auth()->user()->balance > 0)
                        <div class="text-center">
                            Solde :
                            <h2>{{ auth()->user()->balance }}$</h2>
                        </div>
                        @if(isset($win))
                            <div class="alert alert-success">
                                {{ $win }}
                            </div>
                        @endif
                        @if(isset($loss))
                            <div class="alert alert-danger">
                                {{ $loss }}
                            </div>
                        @endif
                        <hr>
                        <form action="" class="row justify-content-center" method="post">
                            @csrf
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="amount">Mise en $</label>
                                    <input type="number" class="form-control"  id="amount" step="0.01" min="0.01" max="{{ Auth::user()->balance }}" name="amount" value="{{ (isset($coinflip->amount) ? $coinflip->amount : '1') }}" required>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="btn-group btn-block">
                                    <button type="button" class="btn btn-success" onclick="min()">Min</button>
                                    <button type="button" class="btn btn-primary" onclick="divideBy2()">/2</button>
                                    <button type="button" class="btn btn-primary" onclick="multiplyBy2()">x2</button>
                                    <button type="button" class="btn btn-danger" onclick="max()">Max</button>
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <button type="submit" class="btn btn-dark btn-block">Jouer</button>
                            </div>
                        </form>
                    @else
                        <div class="text-center">
                            <p>Vous n'avez plus un rond, désolé. Vous pouvez mendier sur le Discord.</p>
                            <a href="{{ url('reload') }}">Clique ici</a> pour remettre ton compte a 100$, limité à une fois toutes les 5 minutes.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        function min() {
            document.getElementById('amount').value = '0.01';
        }

        function max() {
            document.getElementById('amount').value = {{ Auth::user()->balance }};
        }

        function divideBy2() {
            let amount = Number($('#amount').val());
            document.getElementById('amount').value = (amount / 2).toFixed(2);
        }

        function multiplyBy2() {
            let amount = Number($('#amount').val());
            document.getElementById('amount').value = (amount * 2).toFixed(2);
        }

        @if(isset($coinflip->win))
            @if($coinflip->win)
                let audio = new Audio('{{ url('sound/ting.mp3') }}');
            @else
                let audio = new Audio('{{ asset('sound/cowbell.mp3') }}');
            @endif
            audio.play();
        @endif
    </script>
@endpush
