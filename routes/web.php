<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@post');
Route::get('top', 'HomeController@top');

Route::get('reload', function () {
    $user = auth()->user();
    if ($user->balance > 0) {
        return redirect(route('home'));
    }

    $user->balance = 100;
    $user->save();

    return redirect(route('home'));
})->middleware(['auth', 'throttle:1,5']);
