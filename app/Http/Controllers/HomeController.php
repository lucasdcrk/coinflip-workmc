<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use stdClass;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function post(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|min:0.01'
        ]);

        if (Auth::user()->balance < $request->amount) {
            return view('home')->withErrors('T tro pauvr.');
        }

        $coinflip = new stdClass;

        $coinflip->rand = rand(0, 100);
        $coinflip->amount = $request->amount;

        $user = Auth::user();
        $balance = $user->balance;

        if ($coinflip->rand < 50) {
            $user->increment('balance', $coinflip->amount);
            $user->save();

            $coinflip->win = true;

            return view('home')
                ->with('coinflip', $coinflip)
                ->with('win', 'To gagné '.$coinflip->amount.' crédits, sale cheater');
        } else {
            $user->balance = $balance - $coinflip->amount;
            $user->save();

            $coinflip->win = false;

            return view('home')
                ->with('coinflip', $coinflip)
                ->with('loss', 'To perdu '.$coinflip->amount.' crédits, cheh sale merde');
        }
    }

    public function top()
    {
        $users = User::orderByDesc('balance')->limit(10)->get();

        return view('top')->with('users', $users);
    }
}
